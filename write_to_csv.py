# -- coding: UTF-8 --
# https://infinitekind.tenderapp.com/discussions/moneydance-development/2382-connect-to-other-moneyman-data#comment_47170844
# This script dumps oldMoneyDance txns to csv file, forcing keyword to lowercase, which 2nd script imports into newMoneyDance as needed.

from com.infinitekind.moneydance.model import *
import java.util.ArrayList 
import csv
root = moneydance.getCurrentAccount()
book = moneydance.getCurrentAccountBook()
txnset = book.getTransactionSet()
txns = filter(lambda  x:x.class.name == "SplitTxn" and x.getKeywords().size() == 1 and x.getKeywords()[0].encode("utf-8") <> x.getKeywords()[0].encode("utf-8").lower(), txnset) 
csvLines = java.util.ArrayList()
csvLines.add(["Account","ParentAccount","DateInt","Value","Tag"]) 
for txn in txns:
  a = txn.getAccount()
  b = txn.getParentTxn().getAccount()
  c = txn.getDateInt()
  d = txn.getValue()
  e = txn.getKeywords()[0].encode("utf-8").lower()
  newline = [a,b,c,d,e]
  csvLines.add(newline) 
with open("/Users/david/Desktop/txnDump.csv", "w") as f:
  csv.writer(f).writerows(csvLines)
