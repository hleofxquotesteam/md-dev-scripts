# -- coding: UTF-8 --

# https://infinitekind.tenderapp.com/discussions/moneydance-development/2382-connect-to-other-moneyman-data#comment_47170844

from com.infinitekind.moneydance.model import *
import java.util.ArrayList 
import csv 
root = moneydance.getCurrentAccount()
book = moneydance.getCurrentAccountBook()
txnset = book.getTransactionSet()
txns = filter(lambda  x:x.class.name == "SplitTxn" and x.getKeywords().size() == 0, txnset) 
with open("/Users/david/Desktop/txnDump.csv") as myFile:
  reader = csv.DictReader(myFile) 
  for row in reader:
    found = filter(lambda  x:x.getDateInt() == int(row["DateInt"]) and str(x.getAccount()) == row["Account"] and str(x.getParentTxn().getAccount()) == row["ParentAccount"] and x.getValue() == int(row["Value"]), txns)
    if len(found)  == 1:
      tag = java.util.ArrayList() # new empty ArrayList
      tag.add(row["Tag"]) 
      found[0].setKeywords(tag)
